package webumg;

import java.io.Serializable;

import javax.annotation.Resource;
import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.UserTransaction;

import webumg.Usuario;

@Named
@SessionScoped
public class UsuarioControl implements Serializable {
private static final long serialVersionUID = 1L;
	
	private Usuario usuario;
	
	UsuarioControl(){
		usuario = new Usuario();
	}

	@PersistenceContext(unitName = "webumg")
	private EntityManager em;    

	@Resource
	private UserTransaction userTransaction;
	
	public void save() throws Exception  {
	    userTransaction.begin();
	    em.persist(usuario);
	    userTransaction.commit();
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
